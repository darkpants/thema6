#!/usr/bin/env python3
"""
The main purpose of this script is to carry out a terminal command on the Brazil cohort. The only
function in the script furfills this role. The function run_picard_fixmate_bra runs a command that
is defined in the call. This function spawn a process, the process uses a command line tool from
Picard. The FixMateInformation tool command ensures that all information about the mate-pair is
synchronised between each read and it's corresponding mate pair.
"""

__author__ = "Tom Wagenaar"
__date__ = "2018-12-26"

from subprocess import call
import sys
import os

def run_picard_fixmate_bra():
    """
    The only function in this script uses os to look in a folder specefied by a path, furthermore
    it checks if the file ends with .bam. If this is true then the call uses the command line tool
    FixMateInformation from the PicardCommandLine set. Then the right path to the input files is
    specefied and the path to the folder where the output files are written too.
    """
    for bam in os.listdir("/data/storage/students/2018-2019/Thema06/groep_4/picard_readgroup_bra/"):
        if bam.endswith(".bam"):
            call(["PicardCommandLine",
                  "FixMateInformation",
                  "INPUT=/data/storage/students/2018-2019/Thema06/groep_4/picard_readgroup_bra/" + bam,
                  "OUTPUT=/data/storage/students/2018-2019/Thema06/groep_4/picard_fixmateinformation_bra/" + bam
                 ])

def main():
	"""
	Call the only function in the script.
	"""
    run_picard_fixmate_bra()
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))