#!/usr/bin/env python3
"""
This script performs a Trimmomatic on the Brazilian cohort of the Macaque story assignment.
It first searches for all files in the directory which have a .gz extension,
and add these to a list. This list is then sorted. Due to the names of the files,
any file n is a pair with n+1, as long as n is uneven. After that a dictionary is created,
the R1 file is assigned as a key, and the R2 file is assigned as a value.
"""
__author__ = 'Stijn Arends, Hugo Donkerbroek, Rik van de Pol, Tom Wagenaar'

__version__ = "1.0"

# imports
import os
import sys
from subprocess import call


class Catch:
    """
    This class is used to perform Trimmomatic on the Brazilian cohort of
    macaques. Is uses several methods to achieve this.
    """
    def __init__(self):
        self.need_files = self.create_file_list()
        self.create_match()

    def create_file_list(self):
        """
        Creates a list of all items in the directory the script is called, having the
        .gz extension. It then sorts the list, later to be used to create a dictionary.

        :return need_files: A list containing all the files with the .gz extension in the directory
        """
        need_files = []
        # Creates a list of all files in the directory the script is called
        file_list = os.listdir('/commons/Themas/Thema06/Themaopdracht/'
                               'macaque_story/RNA_Microglia/MacaqueBrazil/fastqFiles/')
        for file in file_list:
            # For all items in the list, checks if the extension is .gz
            if file.endswith('.gz'):
                need_files.append(file)
        need_files = sorted(need_files)
        return need_files

    def create_match(self):
        """
        Creates a dictionary of all the files in the need_files list.
        It uses all uneven indexes as a key and all even indexes as a value.
        Then, it uses the trim_files method to trim the files
        using the call function.
        :return:
        """
        # Takes all uneven items in the need files list, and uses it as a key and
        # Use all even items in the need_files list and uses it as a value
        file_dict = dict(zip(self.need_files[::2], self.need_files[1::2]))
        for file in file_dict:
            self.trim_files(file, file_dict[file])
        return file_dict

    def trim_files(self, r_one, r_two):
        """
        Uses the call function, imported from subprocess,
        to perform Trimmomatic on these files.
        :param r_one: The R1 file of the Brazilian cohort
        :param r_two: The R2 file of the Brazilian cohort
        :return:
        """

        call(["TrimmomaticPE",
              "/commons/Themas/Thema06/Themaopdracht/macaque_story/"
              "RNA_Microglia/MacaqueBrazil/fastqFiles/" + r_one,
              "/commons/Themas/Thema06/Themaopdracht/macaque_story/"
              "RNA_Microglia/MacaqueBrazil/fastqFiles/" + r_two,
              "/data/storage/students/2018-2019/Thema06/groep_4/trimmed_fastq_bra/"
              + "forward_paired_" + r_one,
              "/data/storage/students/2018-2019/Thema06/groep_4/trimmed_fastq_bra/"
              + "forward_unpaired_" + r_one,
              "/data/storage/students/2018-2019/Thema06/groep_4/trimmed_fastq_bra/"
              + "reverse_paired_" + r_two,
              "/data/storage/students/2018-2019/Thema06/groep_4/trimmed_fastq_bra/"
              + "reverse_unpaired_" + r_two,
              "ILLUMINACLIP:/usr/share/trimmomatic/TruSeq3-PE.fa:2:30:10",
              "LEADING:28",
              "SLIDINGWINDOW:4:25",
              "MINLEN:67"])


def main():
    """
    Calls the class to perform the trim on the Brazil Macaque files
    :return:
    """
    Catch()


if __name__ == "__main__":
    sys.exit(main())
