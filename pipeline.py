#!/usr/bin/env python3
"""
Pipeline file for processing fastq files
run using following command:

pipeline.py "/commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueNL/fastqFiles/"
"/commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueBrazil/fastqFiles/"
"""

import sys
import os
from subprocess import call
import argparse


__author__ = 'Hugo Donkerbroek'
__version__ = 'V1.0'


def create_dir():
    """
    creates a directory where all the output directories will be placed
    :return: out_dir, str:
        name of the output directory
    """

    # output directory name
    out_dir = 'pipeline_output'

    try:
        # creates the directory
        os.mkdir(out_dir)
        os.chmod(out_dir, 0o777)
        print('Directory "pipeline_output" created..')

        return out_dir

    except FileExistsError:
        print('Directory already exists, please remove directory and try again.')
        sys.exit()


def create_out_dir(out_dir, name):
    """
    function for creating output directories for each of the processing steps
    :param out_dir: str
        working directory
    :param name: str
        name for output directory
    :return: str
        pathway to output files
    """

    # pathway
    dir_name = out_dir + name
    # create directory
    os.mkdir(dir_name)
    print('directory "' + dir_name + '" created.')
    # set permissions
    os.chmod(dir_name, 0o777)

    return dir_name


def run_bowtie(input_dir_nl, input_dir_br, out_dir):
    """
    function for running bowtie2 on the fastq files
    :param input_dir_nl: str
        pathway to the NL cohort
    :param input_dir_br: str
        pathway to the BR cohort
    :param out_dir: str
        pathway to the output directory
    :return:
        returns two strings containing the pathway to the output files of the bowtie2 for the NL and BR cohort
    """

    # name for output directory for output files
    name_nl = '/bowtie_output_nl/'
    name_br = '/bowtie_output_br/'

    # pathway to output files
    bowtie_out_path_nl = create_out_dir(out_dir, name_nl)
    bowtie_out_path_br = create_out_dir(out_dir, name_br)

    # finds the needed files from the NL cohort for the tool
    for fastq in os.listdir(input_dir_nl):
        if fastq.endswith(".fastq") or fastq.endswith(".gz"):
            # renames output files
            out_file_name = bowtie_out_path_nl + fastq
            out_file_name = out_file_name[0:(len(out_file_name) - 5)]
            out_file_name += 'sam'
            # runs bowtie2
            call(["bowtie2",
                  "-x", "/data/storage/students/2018-2019/Thema06/macaque/MacaM_Rhesus_Genome_v7",
                  "-p", 20,
                  "-U", input_dir_nl + fastq,
                  "-S", out_file_name])

    # finds the needed files from the BR cohort for the tool
    for fastq in os.listdir(input_dir_nl):
        if fastq.endswith(".gz"):
            # renames output files
            out_file_name = bowtie_out_path_nl + fastq
            out_file_name = out_file_name[0:(len(out_file_name) - 5)]
            out_file_name += 'sam'
            # runs bowtie
            call(["bowtie2",
                  "-x", "/data/storage/students/2018-2019/Thema06/macaque/MacaM_Rhesus_Genome_v7",
                  "-p", 20,
                  "-U", input_dir_br + fastq,
                  "-S", out_file_name])

    return bowtie_out_path_nl, bowtie_out_path_br


def run_picard_sort(bowtie_path_nl, bowtie_path_br, out_dir):
    """
    function for running the picard tool SortSam
    :param bowtie_path_nl: str
        pathway to the NL output files
    :param bowtie_path_br: str
        pathway to the BR output files
    :param out_dir: str
        pathway to the output directory
    :return:
        returns two strings containing the pathway to the output files of the SortSam for the NL and BR cohort
    """

    # name for output directory for output files
    name_nl = '/picard_sort_output_nl/'
    name_br = '/picard_sort_output_br/'

    # pathway to output files
    picard_sort_out_path_nl = create_out_dir(out_dir, name_nl)
    picard_sort_out_path_br = create_out_dir(out_dir, name_br)

    # finds the needed files from the NL cohort for the tool
    for sam in os.listdir(bowtie_path_nl):
        if sam.endswith(".sam"):
            # renames output files
            out_file_name = picard_sort_out_path_nl + sam
            out_file_name = out_file_name[0:(len(out_file_name) - 3)]
            out_file_name += 'bam'
            # runs picard SortSam
            call(["PicardCommandLine",
                  "SortSam",
                  "INPUT="+bowtie_path_nl + sam,
                  "OUTPUT="+out_file_name,
                  "SORT_ORDER=queryname"])

    # finds the needed files from the BR cohort for the tool
    for sam in os.listdir(bowtie_path_br):
        if sam.endswith(".sam"):
            # renames output files
            out_file_name = picard_sort_out_path_br + sam
            out_file_name = out_file_name[0:(len(out_file_name) - 3)]
            out_file_name += 'bam'
            # runs picard SortSam
            call(["PicardCommandLine",
                  "SortSam",
                  "INPUT="+bowtie_path_br + sam,
                  "OUTPUT="+out_file_name,
                  "SORT_ORDER=queryname"])

    return picard_sort_out_path_nl, picard_sort_out_path_br


def read_groups(picard_sort_out_path_nl, picard_sort_out_path_br, out_dir):
    """
    function for running the picard tool AddOrReplaceReadGroups
    :param picard_sort_out_path_nl: str
        pathway to the NL output files
    :param picard_sort_out_path_br: str
        pathway to the BR output files
    :param out_dir: str
        pathway to the output directory
    :return:
        returns two strings containing the pathway to the output files of the AddOrReplaceReadGroups
        for the NL and BR cohort
    """

    # name for output directory for output files
    name_nl = '/picard_read_groups_output_nl/'
    name_br = '/picard_read_groups_output_br/'

    # pathway to output files
    picard_read_groups_out_path_nl = create_out_dir(out_dir, name_nl)
    picard_read_groups_out_path_br = create_out_dir(out_dir, name_br)

    # finds the needed files from the NL cohort for the tool
    for bam in os.listdir(picard_sort_out_path_nl):
        if bam.endswith(".bam"):
            # renames output files
            out_file_name = picard_read_groups_out_path_nl + bam
            # runs picard AddOrReplaceReadGroups
            call(["PicardCommandLine",
                  "AddOrReplaceReadGroups",
                  "INPUT=" + picard_sort_out_path_nl + bam,
                  "OUTPUT=" + out_file_name,
                  "RGID=", 4,
                  "RGLB=lib1",
                  "RGPL=illumina",
                  "RGPU=unit1",
                  "RGSM=", 20])

    # finds the needed files from the BR cohort for the tool
    for bam in os.listdir(picard_sort_out_path_br):
        if bam.endswith(".bam"):
            # renames output files
            out_file_name = picard_read_groups_out_path_br + bam
            # runs picard AddOrReplaceReadGroups
            call(["PicardCommandLine",
                  "AddOrReplaceReadGroups",
                  "INPUT=" + picard_sort_out_path_br + bam,
                  "OUTPUT=" + out_file_name,
                  "RGID=", 4,
                  "RGLB=lib1",
                  "RGPL=illumina",
                  "RGPU=unit1",
                  "RGSM=", 20])

    return picard_read_groups_out_path_nl, picard_read_groups_out_path_br


def run_picard_fixmate(picard_read_groups_out_path_nl, picard_read_groups_out_path_br, out_dir):
    """
    function for running the picard tool FisMateInformation
    :param picard_read_groups_out_path_nl: str
        pathway to the NL output files
    :param picard_read_groups_out_path_br: str
        pathway to the BR output files
    :param out_dir: str
        pathway to the output directory
    :return:
        returns two strings containing the pathway to the output files of the FixMateInformation
        for the NL and BR cohort
    """

    # name for output directory for output files
    name_nl = '/picard_fixmate_output_nl/'
    name_br = '/picard_fixmate_output_br/'

    # pathway to output files
    picard_fixmate_out_path_nl = create_out_dir(out_dir, name_nl)
    picard_fixmate_out_path_br = create_out_dir(out_dir, name_br)

    # finds the needed files from the NL cohort for the tool
    for bam in os.listdir(picard_read_groups_out_path_nl):
        if bam.endswith(".bam"):
            # renames output files
            out_file_name = picard_fixmate_out_path_nl + bam
            # runs picard FixMateInformation
            call(["PicardCommandLine",
                  "FixMateInformation",
                  "INPUT=" + picard_read_groups_out_path_nl + bam,
                  "OUTPUT=" + out_file_name,
                  "ADD_MATE_CIGAR=true"])

    # finds the needed files from the BR cohort for the tool
    for bam in os.listdir(picard_read_groups_out_path_br):
        if bam.endswith(".bam"):
            # renames output files
            out_file_name = picard_fixmate_out_path_br + bam
            # runs picard FixMateInformation
            call(["PicardCommandLine",
                  "FixMateInformation",
                  "INPUT=" + picard_read_groups_out_path_br + bam,
                  "OUTPUT=" + out_file_name,
                  "ADD_MATE_CIGAR=true"])

    return picard_fixmate_out_path_nl, picard_fixmate_out_path_br


def mark_duplicates(picard_fixmate_out_path_nl, picard_fixmate_out_path_br, out_dir):
    """
    function for running the picard tool MarkDuplicates
    :param picard_fixmate_out_path_nl: str
        pathway to the NL output files
    :param picard_fixmate_out_path_br: str
        pathway to the BR output files
    :param out_dir: str
        pathway to the output directory
    :return:
        returns two strings containing the pathway to the output files of the MarkDuplicates for the NL and BR cohort
    """

    # name for output directory for output files
    name_nl = '/picard_mark_duplicates_output_nl/'
    name_br = '/picard_mark_duplicates_output_br/'

    # pathway to output files
    picard_mark_duplicates_out_path_nl = create_out_dir(out_dir, name_nl)
    picard_mark_duplicates_out_path_br = create_out_dir(out_dir, name_br)

    # finds the needed files from the NL cohort for the tool
    for bam in os.listdir(picard_fixmate_out_path_nl):
        if bam.endswith(".bam"):
            # renames output files
            out_file_name = picard_fixmate_out_path_nl + bam
            out_file_name = out_file_name[0:(len(out_file_name) - 3)]
            out_file_name += 'sam'
            # runs picard MarkDuplicates
            call(["PicardCommandLine",
                  "MarkDuplicates",
                  "INPUT=" + picard_fixmate_out_path_nl + bam,
                  "OUTPUT=" + out_file_name,
                  "METRICS_FILE="
                  "/data/storage/ssamtudents/2018-2019/Thema06/groep_4/picard_markduplicates_nl/tets_metrics.txt"])

    # finds the needed files from the BR cohort for the tool
    for bam in os.listdir(picard_fixmate_out_path_br):
        if bam.endswith(".bam"):
            # renames output files
            out_file_name = picard_fixmate_out_path_br + bam
            out_file_name = out_file_name[0:(len(out_file_name) - 3)]
            out_file_name += 'sam'
            # runs picard MarkDuplicates
            call(["PicardCommandLine",
                  "MarkDuplicates",
                  "INPUT=" + picard_fixmate_out_path_br + bam,
                  "OUTPUT=" + out_file_name,
                  "METRICS_FILE="
                  "/data/storage/ssamtudents/2018-2019/Thema06/groep_4/picard_markduplicates_nl/tets_metrics.txt"])

    return picard_mark_duplicates_out_path_nl, picard_mark_duplicates_out_path_br


def samtools_sort(picard_mark_duplicates_out_path_nl, picard_mark_duplicates_out_path_br, out_dir):
    """
    function for running samtools sort
    :param picard_mark_duplicates_out_path_nl: str
        pathway to the NL output files
    :param picard_mark_duplicates_out_path_br: str
        pathway to the BR output files
    :param out_dir: str
        pathway to the output directory
    :return:
        returns two strings containing the pathway to the output files of the Samtools Sort for the NL and BR cohort
    """

    # name for output directory for output files
    name_nl = '/samtools_sort_output_nl/'
    name_br = '/samtools_sort_output_br/'

    # pathway to output files
    samtools_sort_out_path_nl = create_out_dir(out_dir, name_nl)
    samtools_sort_out_path_br = create_out_dir(out_dir, name_br)

    # finds the needed files from the NL cohort for the tool
    for sam in os.listdir(picard_mark_duplicates_out_path_nl):
        if sam.endswith(".sam"):
            # renames output files
            out_file_name = samtools_sort_out_path_nl + sam
            out_file_name = out_file_name[0:(len(out_file_name) - 3)]
            out_file_name += 'bam'
            # runs samtools sort
            call(["samtools", "sort",
                  picard_mark_duplicates_out_path_nl + sam,
                 "-o", out_file_name])

    # finds the needed files from the BR cohort for the tool
    for sam in os.listdir(picard_mark_duplicates_out_path_br):
        if sam.endswith(".sam"):
            # renames output files
            out_file_name = samtools_sort_out_path_br + sam
            out_file_name = out_file_name[0:(len(out_file_name) - 3)]
            out_file_name += 'bam'
            # runs samtools sort
            call(["samtools", "sort",
                  picard_mark_duplicates_out_path_br + sam,
                  "-o", out_file_name])

    return samtools_sort_out_path_nl, samtools_sort_out_path_br


def feature_counts(samtools_sort_out_path_nl, samtools_sort_out_path_br, out_dir):
    """
    function for running featureCounts
    :param samtools_sort_out_path_nl: str
        pathway to the NL output files
    :param samtools_sort_out_path_br: str
        pathway to the BR output files
    :param out_dir: str
        pathway to the output directory
    :return:
        returns two strings containing the pathway to the output files of the featureCounts for the NL and BR cohort
    """

    # name for output directory
    name = '/feature_counts_output/'

    # creates output directory
    create_out_dir(out_dir, name)

    # create command for running featureCounts
    cmd_line = ["featureCounts",
                "-T", 20,
                "-a", "/data/storage/students/2018-2019/Thema06/"
                      "groep3/annotationfile/MacaM_Rhesus_Genome_Annotation_v7.6.8.gtf",
                "-o", "/feature_counts_output/featureCounts_nl_bra.tx"]

    # adds the NL files to the command
    for sam in os.listdir(samtools_sort_out_path_nl):
        if sam.endswith(".sam"):
            cmd_line.append(sam)

    # adds the BR files to the command
    for sam in os.listdir(samtools_sort_out_path_br):
        if sam.endswith(".sam"):
            cmd_line.append(sam)

    # runs feature counts
    call(cmd_line)


def main():
    """
    runs all the functions in the pipeline to process the files
    """

    # command line arguments required to run the script, contains the pathway to both cohorts
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir_nl',
                        help='Give the path to the directory containing the input data')
    parser.add_argument('input_dir_br',
                        help='Give the path to the directory containing the input data')

    args = parser.parse_args()

    # creates general output directory
    out_dir = create_dir()

    # runs bowtie function
    [bowtie_path_nl, bowtie_path_br] = run_bowtie(args.input_dir_nl,
                                                  args.input_dir_br,
                                                  out_dir)

    # runs picard sort function
    [picard_sort_out_path_nl, picard_sort_out_path_br] = run_picard_sort(bowtie_path_nl,
                                                                         bowtie_path_br,
                                                                         out_dir)

    # runs picard add or replace read groups function
    [picard_read_groups_out_path_nl, picard_read_groups_out_path_br] = read_groups(picard_sort_out_path_nl,
                                                                                   picard_sort_out_path_br,
                                                                                   out_dir)

    # runs picard FixMateInformation function
    [picard_fixmate_out_path_nl, picard_fixmate_out_path_br] = run_picard_fixmate(picard_read_groups_out_path_nl,
                                                                                  picard_read_groups_out_path_br,
                                                                                  out_dir)

    # runs picard MarkDuplicates function
    [mark_duplicates_out_path_nl, mark_duplicates_out_path_br] = mark_duplicates(picard_fixmate_out_path_nl,
                                                                                 picard_fixmate_out_path_br,
                                                                                 out_dir)

    # runs samtools sort function
    [samtools_sort_out_path_nl, samtools_sort_out_path_br] = samtools_sort(mark_duplicates_out_path_nl,
                                                                           mark_duplicates_out_path_br,
                                                                           out_dir)

    # runs featurecounts function
    feature_counts(samtools_sort_out_path_nl, samtools_sort_out_path_br, out_dir)

    return 0


if __name__ == '__main__':
    sys.exit(main())
