#!/usr/bin/env python3
""" """

#MetaDate Variables
__author__ = "Tom Wagenaar"
__version__ = "Version 1.1"
__status__ = "Template"
__date__ = "2018-11-29"

#Imports
from subprocess import call
import sys
import argparse
import os

#Constants
#Class
class Macaque_story():
    """ """

    #Constructor
    def __init__(self):
        """ """

    #Functions
    def run_fastqc_command_nl(self):
        """ This function runs command line arguments """
        for fastq in os.listdir("/commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueNL/fastqFiles/"):
            if fastq.endswith(".fastq"):
                call(["fastqc", "/commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueNL/fastqFiles/" + fastq, "-o", "/data/storage/students/2018-2019/Thema06/groep_4"])

    def run_fastqc_command_bra(self):
        """ This function runs command line arguments """
        for fastq in os.listdir("/commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueBrazil/fastqFiles/"):
            if fastq.endswith(".fastq.gz"):
                call(["fastqc", "/commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueBrazil/fastqFiles/" + fastq, "-o", "/data/storage/students/2018-2019/Thema06/groep_4/Brazilie_cohort_fastqc"])


    def change_permission_nl(self):
        for files in os.listdir("/data/storage/students/2018-2019/Thema06/groep_4"):
            if files.endswith(".zip") or files.endswith(".html"):
                call(["chmod", "777", "/data/storage/students/2018-2019/Thema06/groep_4/" + files])

    def change_permission_bra(self):
        for files in os.listdir("/data/storage/students/2018-2019/Thema06/groep_4/Brazilie_cohort_fastqc"):
            if files.endswith(".zip") or files.endswith(".html"):
                call(["chmod", "777", "/data/storage/students/2018-2019/Thema06/groep_4/Brazilie_cohort_fastqc/" + files])

    def run_trimm_command_nl(self):
        for fastq in os.listdir("/commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueNL/fastqFiles/"):
            if fastq.endswith(".fastq"):
                call(["TrimmomaticSE",
                      "/commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueNL/fastqFiles/" + fastq,
                      "/data/storage/students/2018-2019/Thema06/groep_4/Nederland_cohort_fastqc",
                      "ILLUMINACLIP:/usr/share/trimmomatic/TruSeq3-SE.fa:2:30:10",
                      "SLIDINGWINDOW:4:25",
                       "MINLEN:67"])

#Main
def main(args):
    #Preparation

    #Work
    Macaque = Macaque_story()
    Macaque.run_fastqc_command_nl()
    Macaque.run_fastqc_command_bra()
    Macaque.change_permission_nl()
    Macaque.change_permission_bra()


    #Finalize
    return 0 

if __name__ == "__main__":
    sys.exit(main(sys.argv))

