#!/usr/bin/env python3
"""
The main purpose of this script is to carry out a terminal command on the Brazil cohort. The only
function in the script furfills this role. The function run_picard_markduplicates_bra runs a
command that is defined in the call. This function spawn a process, the process uses a command
line tool from Picard. The MarkDuplicates tool command locates duplicate reads in the bam files
and tags them.
"""

__author__ = "Tom Wagenaar"
__date__ = "2018-12-23"

from subprocess import call
import sys
import os

def run_picard_markduplicates_bra():
    """
    The only function in this script uses os to look in a folder specefied by a path, furthermore
    it checks if the file ends with .bam. If this is true then the call uses the command line tool
    MarkDuplicates from the PicardCommandLine set. Then the right path to the input files is
    specefied and the path to the folder where the output files are written too. It is import that
    the user indentifies the right metrics file with the right path.
    """
    for bam in os.listdir("/data/storage/students/2018-2019/Thema06/groep_4/picard_fixmateinformation_bra/"):
        if bam.endswith(".bam"):
            call(["PicardCommandLine",
                  "MarkDuplicates",
                  "INPUT=/data/storage/students/2018-2019/Thema06/groep_4/picard_fixmateinformation_bra/" + bam,
                  "OUTPUT=/data/storage/students/2018-2019/Thema06/groep_4/picard_markduplicates_bra/mk_" + bam,
                  "METRICS_FILE=/data/storage/students/2018-2019/Thema06/groep_4/picard_markduplicates_bra/metrics_" + bam,
                  "CREATE_INDEX=true"
                 ])

def main():
    run_picard_markduplicates_bra()
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
