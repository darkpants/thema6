#!/usr/bin/env python3
"""
The main purpose of this script is to run multiple command line arguments. The first command line argument is
carried out by the method run_picard_sortsam. The purpose of this command is to sort the input files
by query name, furthermore the input files are in SAM format. The second command line argument is carried out
by the method run_picard_readgroups. This command Adds a read group to the BAM files.
"""

__author__ = "Tom Wagenaar, Stijn Arends"
__version__ = "Version 1.1"
__date__ = "2018-12-21"

from subprocess import call
import sys
import argparse
import os

class Picard_Command_SortSam():
    """
    This class has multiple methods that run command lines using call. Furthermore it looks for files with
    a specific format. Then for every file found the method uses call to run a specific command line
    argument. The output is stored in a folder with a predefined path.
    """

    def __init__(self):
        """ """

    def run_picard_sortsam(self):
        """
        This method runs a tool PicardCommandLine, in this tool there is a command called SortSam. This
        bit of code checks if the're sam files in a specific folder. If there are files that end with sam
        the method then calls the PicardCommandLine tool with the right input, output and other parameters.
        """
        for sam in os.listdir("/data/storage/students/2018-2019/Thema06/bowtie_macaque_NL/"):
            if sam.endswith(".sam"):
                call(["PicardCommandLine",
                      "SortSam",
                      "INPUT=/data/storage/students/2018-2019/Thema06/bowtie_macaque_NL/" + sam,
                      "OUTPUT=/data/storage/students/2018-2019/Thema06/groep_4/picard_tools",
                      "SORT_ORDER=queryname"
                      ])

    def run_picard_readgroups(self):
        """
        This method runs a tool PicardCommandLine, in this tool there is a command called AddOrReplaceReadGroups.
        Next the method gets all the files out a specific folder, if one of those files end with bam the methods
        calls the Picard command. This command has the proper data to carry out the argument. The result of this
        command is that
        """
        for bam in os.listdir("/data/storage/students/2018-2019/Thema06/groep_4/picard_tools/"):
            if bam.endswith(".bam") and not bam.startswith("test"):
                call(["PicardCommandLine",
                      "AddOrReplaceReadGroups",
                      "INPUT=/data/storage/students/2018-2019/Thema06/groep_4/picard_tools/" + bam,
                      "OUTPUT=/data/storage/students/2018-2019/Thema06/groep_4/picard_tools/picard_readgroup/rg_" + bam,
                      "RGID=4",
                      "RGLB=lib1",
                      "RGPL=illumina",
                      "RGPU=unit1",
                      "RGSM=20"
                      ])

def main(args):
    Picard = Picard_Command_SortSam()
    #Picard.run_picard_sortsam()
    Picard.run_picard_readgroups()

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))

