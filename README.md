### file description ###

	FastQC_processing.py			-Python script for running FastQC on all the FastQ files
	
	Macaque_story.py				-Python script for running Trimmomatic (not used)
	
	README.md			 			-This file
	
	hisat_script_BR.py				-Python script for running Hisat on the BR .qz files (not used)
	
	hisat_script_NL.py				-Python script for running Hisat on the NL .qz files (not used)
	
	pca_featurecount.rmd			-Final R script for data visualisation
	
	picard_tool_nl.py				-Python script for running a picard tool command
	
	pipeline.py						-Final Python script for running the complete process -important-
	
	run_picard_fixmate_bra.py		-Python script for running Picard FixMateInformation on the brazil cohort
	
	run_picard_markduplicates.py	-Python script for running Picard MarkDuplicates on the brazil cohort
	
	run_picard_readgroups_bra.py	-Python script for running Picard AddorReplaceReadGroups on the brazil cohort
	
	run_picard_sortsam_bra.py		-Python script for running Picard Sortsam on the brazil cohort
	
	trim.py							-Python scirpt for running Trimmomatic on the brazil cohort (not used)


### Commands for uploading files ###

*	 - git status
*		this will check if your files are synced with the online repository

*	 - git add "filename" (can be multiple files, you can see all the files available for upload with git status)
*		adds a file from your local direcotry for pushing to the online repository

*	 - git commit -m "add your message here"
*		this adds a message to the commit for clarification in the online repository so its clear what changes you have made
*		note: the message will be added to all the files you commit in one go, try using multiple commits for multiple changes for a clear overview

*	 - git push
*		this will push your files to the online repository
	
### Commands for downloading files ###

*	 - git pull
*		this will update all the files if changes were made