#!/usr/bin/env python3
"""
The main purpose of this script is to carry out a terminal command on the Brazil cohort. The only
function in the script furfills this role. The function run_picard_fixmate_bra runs a command that
is defined in the call. This function spawn a process, the process uses a command line tool from
Picard. The AddOrReplaceReadGroups tool command replaces existing read groups or add read groups
to the file.
"""

__author__ = "Tom Wagenaar"
__date__ = "2018-12-23"

from subprocess import call
import sys
import os

def run_picard_readgroup_bra():
    """
    The only function in this script uses os to look in a folder specefied by a path, furthermore
    it checks if the file ends with .bam. If this is true then the call uses the command line tool
    AddOrReplaceReadGroups from the PicardCommandLine set. Then the right path to the input files is
    specefied and the path to the folder where the output files are written too. Furthermore the
    right information about he read groups is added.
    """
    for sam in os.listdir("/data/storage/students/2018-2019/Thema06/groep_4/picard_sort_sam_bra/"):
        if sam.endswith(".sam"):
            call(["PicardCommandLine",
                  "AddOrReplaceReadGroups",
                  "INPUT=/data/storage/students/2018-2019/Thema06/groep_4/picard_sort_sam_bra/" + sam,
                  "OUTPUT=/data/storage/students/2018-2019/Thema06/groep_4/picard_readgroup_bra/" + sam + ".bam",
                  "LB=unknown",
                  "PU=unknown",
                  "SM=unknown",
                  "PL=illumina",
                  "CREATE_INDEX=true"
                 ])


def main():
    run_picard_readgroup_bra()
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
