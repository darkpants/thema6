#!/usr/bin/env python3
"""
the main purpose of this script is to run multiple command line arguments. The first command line argument is
carried out by the method run_picard_sortsam. The purpose of this command is to sort the input files
by query name, furthermore the input files are in SAM format. The second command line argument is carried out
by the method run_picard_readgroups. This command Adds a read group to the BAM files.
"""

__author__ = "Tom Wagenaar"
__version__ = "Version 1.1"
__date__ = "2018-12-23"

from subprocess import call
import sys
import argparse
import os

def run_picard_sortsam_bra():
    """
    This method runs a tool PicardCommandLine, in this tool there is a command called SortSam. This
    bit of code checks if the're sam files in a specific folder. If there are files that end with sam
    the method then calls the PicardCommandLine tool with the right input, output and other parameters.
    """
    for sam in os.listdir("/data/storage/students/2018-2019/Thema06/groep3/bowtie_BZ/"):
        if sam.endswith(".sam") and not sam.startswith("test"):
            call(["PicardCommandLine",
                  "SortSam",
                  "INPUT=/data/storage/students/2018-2019/Thema06/groep3/bowtie_BZ/" + sam,
                  "OUTPUT=/data/storage/students/2018-2019/Thema06/groep_4/picard_sort_sam_bra/sort_" + sam,
                  "SORT_ORDER=queryname"
                 ])

def main():
    run_picard_sortsam_bra()
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
