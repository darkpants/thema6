#!/usr/bin/env python3

import sys
import os
from subprocess import call


def select_files():
    file_list = []
    for fastq in os.listdir(
            "/commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueBrazil/fastqFiles/"):
        if fastq.endswith(".fastq.gz"):
            file_list.append(fastq)
    for files in file_list[0:11]:
        run_hisat(files)


def run_hisat(files):
    call(["hisat2",
          "-x",
          "/data/storage/students/2018-2019/Thema06/groep_4/macaca/macaca",
          "/commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueBrazil/fastqFiles/" + files])


def main():
    select_files()


if __name__ == '__main__':
    sys.exit(main())
